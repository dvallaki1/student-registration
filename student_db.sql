-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 17, 2023 at 01:36 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.0.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `student_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_06_17_080844_create_notifications_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('00f99a23-fe93-48ec-afcf-ca894542d2ec', 'App\\Notifications\\GoodMorningEmail', 'App\\Models\\User', 4, '[\"Good Morning\"]', NULL, '2023-06-17 05:55:46', '2023-06-17 05:55:46'),
('4ad9cb60-6485-4cd9-bc61-4c706a404282', 'App\\Notifications\\GoodMorningEmail', 'App\\Models\\User', 10, '[\"Good Morning\"]', NULL, '2023-06-17 06:01:59', '2023-06-17 06:01:59'),
('58318275-fea5-4ac0-b5eb-3424d6c9ffbe', 'App\\Notifications\\addNewStudent', 'App\\Models\\User', 19, '[\"Your account is successfully created by admin\"]', NULL, '2023-06-17 05:26:36', '2023-06-17 05:26:36'),
('89ceb9c5-28d5-4bf8-bf55-663d154bd1e0', 'App\\Notifications\\UserVerified', 'App\\Models\\User', 3, '[\"Your account is successfully verified by admin\"]', NULL, '2023-06-17 04:09:03', '2023-06-17 04:09:03'),
('9122c07a-cefe-48c6-bea3-c4d979de9a99', 'App\\Notifications\\adminUserRegistration', 'App\\Models\\User', 1, '{\"message\":\"  is registered successfully\",\"url\":\"http:\\/\\/localhost\\/student-registration\\/public\\/admin\\/edit-customer\\/8\"}', NULL, '2023-06-17 02:39:33', '2023-06-17 02:39:33'),
('bb2811c9-a583-46ec-8b9b-1899006facfb', 'App\\Notifications\\UserVerified', 'App\\Models\\User', 5, '[\"Your account is successfully verified by admin\"]', NULL, '2023-06-17 04:23:51', '2023-06-17 04:23:51'),
('d50c1f1d-e5f3-47a9-945e-d7df90fe78cb', 'App\\Notifications\\GoodMorningEmail', 'App\\Models\\User', 5, '[\"Good Morning\"]', NULL, '2023-06-17 06:01:37', '2023-06-17 06:01:37'),
('dc5576c8-4e17-41e3-b40a-b7de5d174424', 'App\\Notifications\\GoodMorningEmail', 'App\\Models\\User', 4, '[\"Good Morning\"]', NULL, '2023-06-17 06:01:30', '2023-06-17 06:01:30'),
('df17f734-25a7-44a0-9e78-6ef284f82ff6', 'App\\Notifications\\HappyBirthdayEmail', 'App\\Models\\User', 5, '[\"Happy Birthday\"]', NULL, '2023-06-17 05:54:19', '2023-06-17 05:54:19'),
('f54772bf-d1c7-4b4a-9850-f8d53e008ce1', 'App\\Notifications\\addNewStudent', 'App\\Models\\User', 10, '[\"Your account is successfully created by admin\"]', NULL, '2023-06-17 03:52:51', '2023-06-17 03:52:51');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'student',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `user_type`, `email`, `dob`, `photo`, `address`, `email_verified_at`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'admin@yopmail.com', '1988-01-11', '', '', NULL, '$2y$10$HrjhG.cMZAsHdcFPxqHoT.aAnWyqbgUTv1lo4AdwbW7OGm4wFCzYy', 0, NULL, '2023-06-17 01:59:54', NULL),
(3, 'student two', 'student', 'student2@yopmail.com', '2005-06-11', 'photos/J0vxNM3lGxqpjxR6oj0UfNIRhuy9cK7XnsVjuKNE.jpg', 'Mihan, Nagpur', NULL, '$2y$10$FNRxzUgQE1lLFID0fE84Nekrt0/.MkGPikwZAtUkC6NZxvjsgrk.6', 0, NULL, '2023-06-17 01:59:55', '2023-06-17 04:21:50'),
(4, 'vallaki', 'student', 'vallaki@yopmail.com', '2005-06-01', 'photos/fnQ4An3jgD1ecchm6xjQSxFXTtp6d5xlR6EH8H9D.jpg', 'test abc address', NULL, '$2y$10$MKQOD7g9SH8mHK6VgNj8Dep0a36QmacxXHkBhG2fkWD3JogMUYn5W', 1, NULL, '2023-06-17 02:31:11', '2023-06-17 04:04:08'),
(5, 'test abc', 'student', 'testabc@yopmail.com', '2005-06-17', '', 's ads s ds df', NULL, '$2y$10$KVieyugs597w5HkY/Fw5luLid5IDk9gkpYYRDF7UDiPK.5af60rNO', 1, NULL, '2023-06-17 02:34:49', '2023-06-17 04:23:46'),
(10, 'test aaa', 'student', 'testsss@yopmail.com', '2005-06-15', 'photos/wp6MOdiEWsoFawxbEolTLk9PBkcFdQ9QLHbNRvJl.jpg', 'a s ds', NULL, '$2y$10$abGvdDRShXAeIRTQtGBGV.AF/tlU2PYHRw4JG3HOzaP5EP8YqBqNG', 1, NULL, '2023-06-17 03:52:46', '2023-06-17 03:52:46'),
(17, 'st one', 'student', 'st1@yopmail.com', '2009-09-19', '', 'test address', NULL, '$2y$10$r/8h.TmMKZzRTqTjUFtDG.jxr0mu/rnSYfLTEtckOB3z/jmZ5l8R.', 0, NULL, '2023-06-17 05:24:38', '2023-06-17 05:24:38'),
(19, 'st two', 'student', 'st2@yopmail.com', '2008-10-20', '', 's asa d', NULL, '$2y$10$elUtmQ/pSupO4l2UYU1YHeCOLR282EB1g.M7bn0BZDuiXXrOKGc2O', 0, NULL, '2023-06-17 05:25:51', '2023-06-17 05:25:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
