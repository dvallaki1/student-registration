<?php

namespace App\Imports;

use App\Models\ManageProduct;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Category;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Models\User;
use App\Notifications\addNewStudent;

class StudentsImport implements ToCollection, WithMultipleSheets
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        $data  = $rows;
        //dd($data);

        $header = $data[0];
        // dd($header);
        unset($data[0]);
        //dd($data);
        try
        {
            foreach ($data as $row)
            {
                $name = $row[0];
                $email = $row[1];
                $dob = $row[2];
                $address = $row[3];
                $password = str_random(8);

                if(!User::where('email',$email)->exists())
                {
                    $student = User::create([
                        'name' => $name,
                        'email' => $email,
                        'dob' => $dob?date('Y-m-d',strtotime($dob)):'',
                        'address' => $address,
                        'photo' => '',
                        'password' => \Hash::make($password)
                    ]);
                    //Notify the student for the registration
                    $student->notify(new addNewStudent($student, $password));
                }
                    
            } 
            
        }catch (\Exception $e) {

            //return back()->with('error','Sorry there is an error in csv format, please check and try again.');
        }
    }

    public function sheets(): array
    {
        return [
            0 => $this,
        ];
    }
    

}
