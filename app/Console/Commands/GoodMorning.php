<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Notifications\GoodMorningEmail;
use App\Models\User;

class GoodMorning extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'good:morning';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Good Morning Email';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::where('status',1)->where('user_type','student')->get();

        foreach ($users as $user) {
            $user->notify(new GoodMorningEmail($user));
        }

        return Command::SUCCESS;
    }
}
