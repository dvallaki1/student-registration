<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Notifications\HappyBirthdayEmail;
use App\Models\User;

class HappyBirthday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'birthday:wish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'HappyBirthday Email';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::where('status',1)
                        ->whereMonth('dob', '=', date('m'))
                        ->whereDay('dob', '=', date('d'))
                        ->get();

        foreach ($users as $user) {
            $user->notify(new HappyBirthdayEmail($user));
        }

        return Command::SUCCESS;
    }
}
