<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;

class UserController extends Controller
{
    public function updateProfile(Request $request)
    {
        $formData = request()->except(['_token']);

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            \File::delete(url('storage/app'.'/'.Auth::user()->photo));

            $formData['photo'] = $file->store('photos');
        }
      
      	if($formData['dob']!='')
            $formData['dob'] = date('Y-m-d',strtotime($formData['dob']));

        User::find(Auth::id())->update($formData);

        return back()->with('success', 'Profile updated successfully.');

    }
}
