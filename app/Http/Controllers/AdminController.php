<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Notifications\addNewStudent;
use App\Notifications\UserVerified;
use Hash;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\StudentsImport;
use Validator;

class AdminController extends Controller
{
    public function manageStudents()
    {
        $result = User::whereUserType('student')->get();
        return view('admin.students.manage', compact('result'));
    }

    public function addStudent()
    {
        return view('admin.students.add');
    }

    public function saveStudent(Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],

        ], [
            "name.required" => "Please enter :attribute",
            "email.required" => "Please enter :attribute",
            'password.confirmed'=>'The proposed passwords do not match.'
        ]);

        $password = $formData['password'];
        $formData['password'] = Hash::make($formData['password']);

        $formData['status'] = 1;

        if ($request->hasFile('photo')) {
            $allowedfileExtension = ['jpeg', 'jpg', 'png', 'svg'];
            $file = $request->file('photo');

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['photo'] = $file->store('photos');
            }
            else
                return back()->with('error','Image format invalid');
        }

        if($formData['dob']!='')
            $formData['dob'] = date('Y-m-d',strtotime($formData['dob']));

        $student = User::create($formData);

        //Notify the student for the registration
        $student->notify(new addNewStudent($student, $password));

        if ($student->save()) {
            return redirect('/admin/manage-students')->with('success', 'Student added successfully.');
        } else {
            return redirect('/admin/manage-students')->with('error', 'Sorry there is an error while adding student. please try again.');
        }
    }

    public function editStudent($id)
    {
        $row = User::find($id);
        return view('admin.students.edit', compact('row'));
    }

    public function updateStudent($id, Request $request)
    {
        $formData = request()->except(['_token']);

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],

        ], [
            "name.required" => "Please enter :attribute",
            "email.required" => "Please enter :attribute",
        ]);

        // save the row to the database
        $duplicateEntry = User::whereEmail($formData['email'])->first();

        if ($duplicateEntry == '' || ($duplicateEntry != '' && $duplicateEntry->id == $id)) {
            if ($request->hasFile('photo')) {
                $allowedfileExtension = ['jpeg', 'jpg', 'png', 'svg'];
                $file = $request->file('photo');

                $extension = $file->getClientOriginalExtension();

                if (in_array($extension, $allowedfileExtension)) {

                    $formData['photo'] = $file->store('photos');
                }
                else
                    return back()->with('error','Image format invalid');
            }

            if($formData['dob']!='')
                $formData['dob'] = date('Y-m-d',strtotime($formData['dob']));
            
            User::find($id)->update($formData);

        } else
            return redirect()->back()->with('error', 'Email already taken.');

        return redirect('/admin/manage-students')->with('success', 'Student updated successfully.');
    }

    public function deleteStudent(Request $request)
    {
        $id = $request->input('id');

        if (User::find($id)->delete()) {
            return response()->json(['status' => 'success', 'msg' => 'You have successfully deleted student']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error in deleting student. Please try again later!']);
        }
    }

    public function activateDeactivateStudent(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
            
        if($status == 1)
            $msg = 'You have successfully activated the student.';
        else
            $msg = 'You have successfully deactivated the student.';

        $user = User::find($id);
        if ($user->update(['status'=>$status])) {
            if($status==1)
            {
                $user->notify(new UserVerified($user));
            }
            return response()->json(['status' => 'success', 'msg' => $msg]);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Sorry there is an error while changing the status of student. Please try again later!']);
        }
    }

    public function uploadStudents()
    {
        return view('admin.students.upload');
    }

    public function uploadStudentsSave(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'upload_csv' => 'required|mimes:csv',
            )
        );

        if ($validator->fails())
        {
           return back()->with('error','The product csv must be a file of type: csv');
        }
        Excel::import(new StudentsImport(), $request->upload_csv);
        return back()->with('success','Students csv imported successfully');
    }
}
