<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $input = $request->all();
   
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
   
        $user = User::where('email',$request->email)->first();

        if ($user!='' && \Hash::check($request->password, $user->password))
        {
            if ($user->user_type == 'admin') {
                Auth::login($user);
                return redirect('admin/manage-students');
            }
            elseif($user->status==1)
            {
                Auth::login($user);
                return redirect('profile');
            }
            else
                return redirect('login')->with('error','Admin will verify your account soon. Please try after some time.');
        }else{
            return redirect('login')->with('error','Invalid Login.');
        }
    }
    
    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function forgetPassword(Request $request)
    {
        $request->validate(['email' => ['required', 'email']]);
        if ($user = User::where('email', $request->email)->where('user_type','!=','admin')->first()) {
            $token = str_random(60);
            $password_reset_user = DB::table('password_resets')
                ->where('email', $request->email)
                ->first();
            if ($password_reset_user) {
                $token_saved = DB::table('password_resets')
                    ->where('email', $password_reset_user->email)
                    ->update([
                        'token' => $token]);
            } else {
                $token_saved = DB::table('password_resets')->insert(['email' => $request->email,
                    'token' => $token, 'created_at' => date('Y-m-d H:i:s')]);
            }
            if ($token_saved) {
                $user->notify(new sendPasswordLink($user,$token));
                return back()->with('success', 'Please check your email for password reset instructions.');
            } else {
                return back()->with('error', 'This email does not exist.');
            }
        } else {
            return back()->with('error', 'This email does not exist.');
        }
    }

    public function updateForgotPassword(Request $request)
    {

        $validation = $this->validate($request, ['password' => 'required|min:8|confirmed', 'password_confirmation' => 'required']);
        $email = DB::table('password_resets')
            ->select('email')
            ->where('token', $request->token)
            ->first();
        $user = DB::table('users')
            ->select('*')
            ->where('email', $email->email)
            ->first();
        if ($request->password == $request->password_confirmation) {
            if ($user) {
                $password_updated = DB::table('users')
                    ->where('email', $user->email)
                    ->update(['password' => Hash::make($request->password)]);

                if ($password_updated) {
                    return redirect('/login')->with(['success' => 'Password was changed successfully.']);
                } else {
                    return redirect('/login')->with(['error' => 'There is an error while changing the password please try again later.!']);
                }
            }
        } else {
            return back()->with('error', 'Password do not matched with confirm password');
        }
    }
}
