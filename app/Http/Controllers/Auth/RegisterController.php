<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Notifications\adminUserRegistration;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function showSignupForm()
    {
      return view('auth.register');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ],[
            'email.unique'=>'The email is already registered on this site.',
            'password.confirmed'=>'The proposed passwords do not match.'
        ]);
    }

    protected function register(Request $request)
    {
        $formData = request()->except(['_token']);

        // validate the user form data
        $validation = $this->validator($formData);

        // if validation fails
        if ($validation->fails()) {
            // redirect back to the form 
            return back()->withErrors($validation)->withInput();
        }

        // if validation passes
        $password = $formData['password'];
        $formData['password'] = Hash::make($formData['password']);

        if ($request->hasFile('photo')) {
            $allowedfileExtension = ['jpeg', 'jpg', 'png', 'svg'];
            $file = $request->file('photo');

            $extension = $file->getClientOriginalExtension();

            if (in_array($extension, $allowedfileExtension)) {

                $formData['photo'] = $file->store('photos');
            }
            else
                return back()->with('error','Image format invalid');
        }

        if($formData['dob']!='')
            $formData['dob'] = date('Y-m-d',strtotime($formData['dob']));

        // save the user to the database
        $user = User::create($formData);

        //Notify the admin for the registration
        $admin = User::whereUserType('admin')->first();
        $admin->notify(new adminUserRegistration ($user,$admin));

        // return a view 
        return redirect('/login')->with('success', 'Registration successful.');
    }
}
