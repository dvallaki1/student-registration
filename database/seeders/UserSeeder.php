<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;
use Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        
        $users = [
            [  'name' => 'admin',
                'email' => 'admin@yopmail.com',
                'password' => Hash::make('123456789'),
                'user_type' => 'admin',
                'dob' => '1988-01-11',
                'address' => '',
                'photo' => '',
                'created_at' => date("Y-m-d h:i:s")],
            [  'name' => 'student one',
                'email' => 'student1@yopmail.com',
                'password' => Hash::make('123456789'),
                'user_type' => 'student',
                'dob' => '1989-10-12',
                'address' => 'Sadar, Nagpur',
                'photo' => '',
                'created_at' => date("Y-m-d h:i:s")],
            [  'name' => 'student two',
                'email' => 'student2@yopmail.com',
                'password' => Hash::make('123456789'),
                'user_type' => 'student',
                'dob' => '1992-01-20',
                'address' => 'Mihan, Nagpur',
                'photo' => '',
                'created_at' => date("Y-m-d h:i:s")]

        ];
        DB::table('users')->insert($users);
    }
}
