<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//login/register
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login')->name('login.submit');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/register', 'Auth\RegisterController@showSignupForm')->name('register');
Route::post('/register', 'Auth\RegisterController@register')->name('register.submit');


Route::middleware('auth')->group(function () {

    Route::get('/profile', function () {return view('student.profile');});
    Route::post('/profile', 'UserController@updateProfile');
    
    Route::middleware('is_admin')->prefix('admin')->group(function () {

        Route::get('/', 'AdminController@index')->name('admin.dashboard');
       
        //Manage students
        Route::get('/manage-students', 'AdminController@manageStudents');
        Route::get('/add-student', 'AdminController@addStudent');
        Route::post('/add-student', 'AdminController@saveStudent');
        Route::get('/edit-student/{id?}', 'AdminController@editStudent');
        Route::post('/edit-student/{id?}', 'AdminController@updateStudent');
        Route::post('/delete-student', 'AdminController@deleteStudent');

        Route::get('upload-students', 'AdminController@uploadStudents');
        Route::post('upload-students', 'AdminController@uploadStudentsSave');

        Route::post('/activate-deactivate-student', 'AdminController@activateDeactivateStudent');
    });
});


