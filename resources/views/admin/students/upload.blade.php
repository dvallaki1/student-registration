@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Upload Student</div>

                <div class="card-body">
            
                    <form action="{{URL::to('admin/upload-students')}}" method="post"
                                  enctype="multipart/form-data" data-parsley-validate>
                        @csrf
                        <!-- Modal Header -->
                        <div class="modal-header"
                             style="font-size: 22px; border-bottom: solid 1px #ccc; margin: 0 0 26px;">
                            Upload Subjects
                        </div>

                        <!-- Modal body -->
                            <div class="form-group">
                                <input type="file" name="upload_csv" class="form-control"
                                       data-parsley-fileextensionCsv="csv" required="" 
                                       >
                            </div>
                            <p class="text-left">
                                Please click the <a
                                        href="{{ URL::asset('/') }}/assets/admin/sample_students.csv"
                                        download="">sample CSV file</a> here to import business listing.
                            </p>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <a class="btn btn-danger" href="{{url('admin/manage-students')}}">Cancel</a>
                            <button type="submit" class="btn btn-primary">Upload CSV</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection

    
