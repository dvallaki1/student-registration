@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Student</div>

                <div class="card-body">
                    <form action="" method="post"
                            id="add_business_term_form" enctype="multipart/form-data" data-parsley-validate>
                        @csrf
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="name">Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="name"  placeholder="Enter name" name="name" required="" data-parsley-required-message="Please enter name." value="{{$row->name}}">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="enail">Email<span class="text-danger">*</span></label>
                                    <input type="email" class="form-control" id="email"  placeholder="Enter email" name="email" required=""  data-parsley-required-message="Please enter email."data-parsley-type-message="Please enter a valid email." value="{{$row->email}}" readonly="">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="name">DOB<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="dob"  placeholder="Enter dob" name="dob" required="" data-parsley-required-message="Please enter dob." value="{{$row->dob?date('m/d/Y',strtotime($row->dob)):''}}">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="enail">Photo<span class="text-danger">*</span></label>
                                    @if($row->photo!='')
                                    <img src="{{url('storage/app'.'/'.$row->photo)}}" style="height: 100px;width: 100px">
                                    @endif
                                    <input type="file" class="form-control" id="photo"  placeholder="Enter photo" name="photo" @if($row->photo=='') required="" @endif data-parsley-required-message="Please select photo." data-parsley-fileextension="jpg,jpeg,png,svg">
                                </div>
                            </div>
                            
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="address">Address<span class="text-danger">*</span></label>
                                    <textarea class="form-control" id="address"  placeholder="Enter address" name="address" required data-parsley-required-message="Please enter address">{{$row->address}}</textarea>
                                </div>
                            </div>
                          
                        </div>

                        <input type="submit" class="btn btn-danger" value="Save">

                        <a href="{{Request::root()}}/admin/manage-students" class="btn btn-danger btn-gold-styled pull-right">Cancel</a>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $( function() {
        $( "#dob" ).datepicker({
            maxDate: "-18Y"
        });
    } );
</script>
@endsection

    
