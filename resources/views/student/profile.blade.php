@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Update Profile</div>

                <div class="card-body">
                    <form method="post" data-parsley-validate enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="name">Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="name"  placeholder="Enter name" name="name" required="" data-parsley-required-message="Please enter name." value="{{Auth::user()->name}}">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="">Email<span class="text-danger">*</span></label>
                                    <input type="email" class="form-control" id="email"  placeholder="Enter email" name="email" required=""  data-parsley-required-message="Please enter email."data-parsley-type-message="Please enter a valid email." value="{{Auth::user()->email}}" readonly="">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="name">DOB<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="dob"  placeholder="Enter dob" name="dob" required="" data-parsley-required-message="Please enter dob." value="{{Auth::user()->dob?date('m/d/Y',strtotime(Auth::user()->dob)):''}}">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="">Photo<span class="text-danger">*</span></label>
                                    @if(Auth::user()->photo!='')
                                    <img src="{{url('storage/app'.'/'.Auth::user()->photo)}}" style="height: 100px;width: 100px">
                                    @endif
                                    <input type="file" class="form-control" id="photo"  placeholder="Enter photo" name="photo" @if(Auth::user()->photo=='') required="" @endif data-parsley-required-message="Please select photo." data-parsley-fileextension="jpg,jpeg,png,svg">
                                </div>
                            </div>
                            
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="address">Address<span class="text-danger">*</span></label>
                                    <textarea class="form-control" id="address"  placeholder="Enter address" name="address" required data-parsley-required-message="Please enter address" >{{Auth::user()->address}}</textarea>
                                </div>
                            </div>
                            
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary blueBtn smallBtn">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $( function() {
        $( "#dob" ).datepicker({
            maxDate: "-18Y"
        });
    } );
</script>

@endsection
