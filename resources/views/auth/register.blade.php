@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="post" data-parsley-validate enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="name">Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="name"  placeholder="Enter name" name="name" required="" data-parsley-required-message="Please enter name." value="{{old('name')}}">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="enail">Email<span class="text-danger">*</span></label>
                                    <input type="email" class="form-control" id="email"  placeholder="Enter email" name="email" required=""  data-parsley-required-message="Please enter email."data-parsley-type-message="Please enter a valid email." value="@if (!$errors->has('email')){{old('email')}}@endif" >
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="name">DOB<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="dob"  placeholder="Enter dob" name="dob" required="" data-parsley-required-message="Please enter dob." value="{{old('dob')?date('m/d/Y',strtotime(old('dob'))):''}}">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="enail">Photo<span class="text-danger">*</span></label>
                                    <input type="file" class="form-control" id="photo"  placeholder="Enter photo" name="photo" required=""  data-parsley-required-message="Please select photo." data-parsley-fileextension="jpg,jpeg,png,svg">
                                </div>
                            </div>
                            
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="address">Address<span class="text-danger">*</span></label>
                                    <textarea class="form-control" id="address"  placeholder="Enter address" name="address" required data-parsley-required-message="Please enter address"></textarea>
                                </div>
                            </div>
                          
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="password">Password<span class="text-danger">*</span></label>
                                    <input type="password" class="password form-control" id="password" placeholder="Password" name="password" required data-parsley-required-message="Please enter password." value="@if (!$errors->has('password')){{old('password')}}@endif">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="reEnterPassword">Confirm Password<span class="text-danger">*</span></label>
                                    <input type="password" class="password form-control" id="reEnterPassword" name="password_confirmation" data-parsley-equalto="#password" data-parsley-equalto-message="Password and confirm password should be same." required data-parsley-required-message="Please re-enter password." placeholder="Re-enter password" value="@if (!$errors->has('password_confirmation')){{old('password_confirmation')}}@endif">
                                   
                                </div>
                            </div>
                            
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary blueBtn smallBtn">Register</button>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="lbBtmTxt">Already have an account with us, <a href="{{url('login')}}">Login</a></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $( function() {
        $( "#dob" ).datepicker({
            maxDate: "-18Y"
        });
    } );
</script>

@endsection
